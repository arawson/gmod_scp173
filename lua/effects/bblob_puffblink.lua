function EFFECT:Init( data )
	local vOffset = data:GetOrigin() + Vector( 0, 0, 0.2 )

	local emitter = ParticleEmitter(vOffset)
		for i=0, 400 do
			local particle = emitter:Add( "particle/smokesprites_0001", vOffset )
			if particle then
				-- particle:SetAngles( vAngle )
				particle:SetVelocity( Vector( math.random(-2,2), math.random(-2,2), math.random(0, 3) ) )
                particle:SetRollDelta(math.random())
                local r = math.random(0, 80)
				particle:SetColor( r, r, r )
				particle:SetLifeTime( 0 )
				particle:SetDieTime( math.random() )
				particle:SetStartAlpha( 200 )
				particle:SetEndAlpha( 0 )
                particle:SetStartLength(100)
				particle:SetStartSize( 80 )
				particle:SetStartLength( 1 )
				particle:SetEndSize( 120 )
				particle:SetEndLength( 120 )
			end
		end
	emitter:Finish()
end

function EFFECT:Think()
	return false
end

function EFFECT:Render()
	return false
end
