AddCSLuaFile()

-- Notes:
-- scp 173 may not be possible with the nextbot ai
--   it can't freeze the moment a player looks at it, and is very often still moving

--alternate idea:
--a "dark templar" that stalks a target player throughout the map
-- except it only cloaks when it is spotted

--on start, pick a player to target at random and give that player a signal, maybe a sound
--start stalking the player (target playerPos - playerEyeVector * c)
--when seen, stop stalking
--  display "panic animation"
--  find target location for scatter behavior, preferably one with low light
--  start running away
--  cloak (or zeratul blink animation with black fog)
--  start rechase timer
--when rechase timer ends, trigger start behavior
--nice touches
-- play more sounds as player gets closer
-- play loud slash on killing the player

ENT.Base = "base_nextbot"
ENT.spawnable = true

function ENT:Initialize()
    self:SetModel("models/new173/new173.mdl")

    self.LoseTargetDist = 2000
    self.SearchRadius = 1000

    self.COOLDOWN_TIME = 0.002
    self.THINK_DELAY = 0.001

    self.Width = self:BoundingRadius() * 0.5

    -- timer.Create("gameoverman",1.5,1,function() local scale = Vector(1,1,4)
    --     local mat = Matrix()
    --     mat:Scale(scale)
    --     if (self:IsNPC()) then
    --         print('OH BOY, THIS IS AN NPC')
    --         self:EnableMatrix("RenderMultiply",mat)
    --     else
    --         print('OH NOES THIS ISNT AN NPC')
    --     end
    -- end)
end

----------------------------------------------------
--Find out if we are visible
function ENT:HasLOS(plyr)
    local tr = util.TraceLine({
        start = self:GetPos(),
        endpos = plyr:LocalToWorld(plyr:OBBCenter()),
        filter = {self},
        mask = CONTENTS_SOLID, CONTENTS_OPAQUE, CONTENTS_MOVEABLE
    })

    if tr.Fraction > 0.98 then return true end
    return false
end

function ENT:CanSeeUs(plyr)
    local ViewEnt = plyr:GetViewEntity()
    if ViewEnt == plyr and not self:HasLOS(plyr) then return true end

    local fov = plyr:GetFOV() - 15
    local Disp = self:GetPos() - ViewEnt:GetPos()
    local Dist = Disp:Length()

    local MaxCos = math.abs(
        math.cos(
            math.acos(Dist/math.sqrt(Dist*Dist + self.Width*self.Width))
            + fov * (math.pi/180)
        )
    )

    if Disp:Dot(ViewEnt:EyeAngles():Forward()) > MaxCos then
        return true
    end
end

function ENT:IsVisible()
    local plys = player.GetAll()
    --print ("how many players? " .. #plys)
    local isVisible = false

    for i, p in ipairs(plys) do
        if not IsValid(p) or not p:Alive() then
        else
            -- if self:CanSeeUs(p) then print("can see") else print("can't see") end
            -- if self:HasLOS(p) then print("has los") else print ("no los") end
            isVisible = isVisible or (self:CanSeeUs(p) and self:HasLOS(p))
        end
    end

    return isVisible
end

----------------------------------------------------
-- ENT:Get/SetEnemy()
-- Simple functions used in keeping our enemy saved
----------------------------------------------------
function ENT:SetEnemy( ent )
	self.Enemy = ent
end
function ENT:GetEnemy()
	return self.Enemy
end

----------------------------------------------------
-- ENT:HaveEnemy()
-- Returns true if we have a enemy
----------------------------------------------------
function ENT:HaveEnemy()
	-- If our current enemy is valid
	if ( self:GetEnemy() and IsValid( self:GetEnemy() ) ) then
		-- If the enemy is too far
		if ( self:GetRangeTo( self:GetEnemy():GetPos() ) > self.LoseTargetDist ) then
			-- If the enemy is lost then call FindEnemy() to look for a new one
			-- FindEnemy() will return true if an enemy is found, making this function return true
			return self:FindEnemy()
		-- If the enemy is dead( we have to check if its a player before we use Alive() )
		elseif ( self:GetEnemy():IsPlayer() and !self:GetEnemy():Alive() ) then
			return self:FindEnemy()		-- Return false if the search finds nothing
		end
		-- The enemy is neither too far nor too dead so we can return true
		return true
	else
		-- The enemy isn't valid so lets look for a new one
		return self:FindEnemy()
	end
end

----------------------------------------------------
-- ENT:FindEnemy()
-- Returns true and sets our enemy if we find one
----------------------------------------------------
function ENT:FindEnemy()
	-- Search around us for entities
	-- This can be done any way you want eg. ents.FindInCone() to replicate eyesight
	local _ents = ents.FindInSphere( self:GetPos(), self.SearchRadius )
	-- Here we loop through every entity the above search finds and see if it's the one we want
	for k, v in pairs( _ents ) do
		if ( v:IsPlayer() ) then
			-- We found one so lets set it as our enemy and return true
			self:SetEnemy( v )
			return true
		end
	end
	-- We found nothing so we will set our enemy as nil ( nothing ) and return false
	self:SetEnemy( nil )
	return false
end

function ENT:RunBehaviour()
    self.loco:SetAcceleration(10000)
    while (true) do

        if self:IsVisible() then
            --don't do anything
            --don't even breathe
            self.loco:SetDesiredSpeed(0)
            --print ("no no")
        else
            --print ("fucking murder time")
            self.loco:SetDesiredSpeed(100)
            if (self:HaveEnemy()) then
                self.loco:FaceTowards(self:GetEnemy():GetPos())
                self:MoveToPos( self:GetPos() + Vector( math.Rand( -1, 1 ), math.Rand( -1, 1 ), 0 ) * 400 )
            else
                self:MoveToPos( self:GetPos() + Vector( math.Rand( -1, 1 ), math.Rand( -1, 1 ), 0 ) * 400 )
            end
        end

        coroutine.wait(0.01)
    end
end

function ENT:Think_me_a_badgers()

    if self:IsVisible() then
        self:Calm()
        self:NextThink(CurTime() + self.COOLDOWN_TIME)
    else
        self:Antagonize()
        self:NextThink(CurTime() + self.THINK_DELAY)
    end

    return true
end

list.Set( "NPC", "bblob_scp173", {
	Name = "SCP 173 (Alpha)",
	Class = "bblob_scp173",
	Category = "binaryblob"
} )
