AddCSLuaFile()
AddCSLuaFile( "effects/bblob_puffblink.lua" )

--a psychotic stalker that's afraid of being seen

ENT.Base = "base_nextbot"
ENT.Spawnable = true
ENT.AdminSpawnable = true
ENT.RenderGroup = RENDERGROUP_TRANSLUCENT

local MODE_INVALID = 1
local MODE_STALK = 2
local MODE_FLEE = 3
local MODE_KILL = 4
local MODE_FLEE2 = 5

local MODE_MAX = MODE_FLEE2

--how close do we consider beeing directly looked at?
local VIEW_EPSILON = math.cos( math.rad(15) )

local STALK_COEFFICIENT = 50 --how many units behind the player view are we
                             --targetting?
local STALK_EPSILON = 30 --how close do we have to be to kill?
local STALK_FOUND_SOUNDS = {
    'stalk/foundyou1.wav'
}
local STALK_CREEP_SOUNDS = {
}
local STALK_SPEED = 150
local STALK_ACCELERATION = 100

local FLEE1_ANIM_TIME = 3
local FLEE1_CLOAK_TIME = 5
local FLEE_BLINK_DELAY = 0.5
local FLEE2_TIME = 10
local CLOAK_ALPHA = 50
local FLEE_COORDS_MAX_RETRIES = 10
local BLINK_AWAY_DIST = 1000
local FLEE_SPEED = 400
local FLEE_ACCELERATION = 200
local COWER_ACCELERATION = 1000

function ENT:Initialize()
	self:SetModel( "models/combine_soldier_prisonguard.mdl" )
    self:SetRenderMode(RENDERMODE_TRANSALPHA)

	self.LoseTargetDist = 2000
	self.SearchRadius = 1000

	self.Width = self:BoundingRadius() * 0.5
    self.Height = self:BoundingRadius() * 2

	self.mode = MODE_INVALID
	self.nextMode = MODE_INVALID --next mode is used to determine if a mode
							   --transition is needed
    self.lastMode = MODE_INVALID

    self.cloaked = false

    self.lastSeenBy = nil --who are we fleeing from?

    self.flee1AnimEndTime = 0.0
    self.flee1BlinkTime = 0.0
    self.flee1HasBlinked = false
    self.flee2ReturnToStalkTime = 0.0

    self.stalkerDecloakTimerName = "stalkerDecloak" .. self:EntIndex()
    timer.Create(self.stalkerDecloakTimerName,
        FLEE1_CLOAK_TIME,
        1,
        function() self:SetCloaked(false) end)
end

function ENT:OnRemove()
    timer.Remove(self.stalkerDecloakTimerName)
end

----------------------------------------------------
--ENT:FindGround()
--find the ground so we dont frickin teleport into it
--return nil if we pass out of our hard-coded limits
--other functions can use that nil to know when to give up at life
----------------------------------------------------
function ENT:FindGround(coords, minheight)
    --the plan is to take the target coords and jump upwards, trace down
    --  after tracing down we should be able to calculate height
    local MAX_TRIES = 15
    local HEIGHT_DIV = 3
    local goodCoords = nil
    local delta = Vector(0, 0, minheight/HEIGHT_DIV)

    local endpos = coords
    local start = coords + Vector(0, 0, minheight)
    local filter = function(ent)
        if (ent:GetClass() == "prop_physics") then return true end end

    local try = 0
    while try < MAX_TRIES do
        try = try + 1
        local tr = util.TraceLine({
            start = start, endpos = endpos, filter = filter,
            mask = CONTENTS_SOLID, CONTENTS_OPAQUE, CONTENTS_MOVEABLE
        })

        if tr.Hit then
            endpos = endpos + delta
            start = start + delta
        else
            goodCoords = endpos
            try = MAX_TRIES
        end
    end

    if goodCoords then
        print('good result')
    else
        print('no good coords')
    end

    return goodCoords
end

----------------------------------------------------
--ENT:CalcFleeCoords()
--return random coordinates away from the players
--or random coordinates if we run out of tries
----------------------------------------------------
function ENT:CalcFleeCoords()
    local worldEnt = game.GetWorld()
    local width = worldEnt:BoundingRadius()

    return Vector(0,0,0)
end

----------------------------------------------------
--ENT:HasFlee1Blinked/SetFlee1Blinked
--track if we've already blunked
----------------------------------------------------
function ENT:HasFlee1Blinked()
    return self.flee1HasBlinked
end
function ENT:SetFlee1Blinked(blunk)
    if blunk then self.flee1HasBlinked = true else self.flee1HasBlinked = false end
end

----------------------------------------------------
--ENT:GetSeenByBlinkAwayTarget()
--calculate a point away from the player that has seen us
----------------------------------------------------
function ENT:GetSeenByBlinkAwayTarget()
    local pos = self:GetLastSeenBy():GetViewEntity():EyePos()
    local view = self:GetLastSeenBy():GetViewEntity()

    local eye = view:EyeAngles():Forward():GetNormalized()

    local r = (eye * BLINK_AWAY_DIST) + pos

    r = self:FindGround(r, self.Height)

    if r == nil then
        --this is bad
        r = Vector(0,0,0)
        print('warning bad blink target')
    end

    return r
end

----------------------------------------------------
--ENT:SetFlee2ReturnToStalkTime/IsFlee2ReturnToStalk
--set when to return to stalking
--or check if we should return to stalking
----------------------------------------------------
function ENT:SetFlee2ReturnToStalkTime()
    self.flee2ReturnToStalkTime = CurTime() + FLEE2_TIME
end
function ENT:IsFlee2ReturnToStalk()
    return CurTime() > self.flee2ReturnToStalkTime
end

----------------------------------------------------
--ENT:SetFlee1BlinkTime/IsFlee1BlinkTime
--set when to Blink
--or check if  we should blink
----------------------------------------------------
function ENT:SetFlee1BlinkTime()
    self.flee1BlinkTime = CurTime() + FLEE_BLINK_DELAY
end
function ENT:IsFlee1BlinkTime()
    return CurTime() > self.flee1BlinkTime
end

----------------------------------------------------
--ENT:SetFlee1AnimEndTime/IsFlee1AnimEndTimeUp
--set when to end the cowering animation
--or get if we are done cowering
----------------------------------------------------
function ENT:SetFlee1AnimEndTime()
    self.flee1AnimEndTime = CurTime() + FLEE1_ANIM_TIME
end
function ENT:IsFlee1AnimEndTimeUp()
    return CurTime() > self.flee1AnimEndTime
end


----------------------------------------------------
--ENT:Get/SetCloaked()
--get or set the cloaking state
----------------------------------------------------
function ENT:GetCloaked()
    return self.cloaked
end
function ENT:SetCloaked(cloaked)
    local c = self:GetColor()
    if cloaked then
        self.cloaked = true
        c.a = CLOAK_ALPHA
    else
        self.cloaked = false
        c.a = 255
    end
    self:SetColor(c)
end

----------------------------------------------------
--ENT:DelayedDecloak()
--decloaks us later
----------------------------------------------------
function ENT:DelayedDecloak()
	timer.Create(self.stalkerDecloakTimerName,
        FLEE1_CLOAK_TIME,
        1,
        function() self:SetCloaked(false) end)
end

----------------------------------------------------
--ENT:CancelDecloak()
--dont decloak us later
----------------------------------------------------
function ENT:CancelDecloak()
    timer.Stop(self.stalkerDecloakTimerName)
end

----------------------------------------------------
--ENT:Get/SetMode()
--get or set the mode of the stalker
----------------------------------------------------
function ENT:GetMode()
	return self.mode
end
function ENT:SetMode(mode)
	if mode < MODE_INVALID or mode > MODE_FLEE2 then
		self.mode = MODE_INVALID
	else
		self.mode = mode
	end
end

----------------------------------------------------
--ENT:Get/SetNextMode()
--get or set the next mode to go to
----------------------------------------------------
function ENT:GetNextMode()
    return self.nextMode
end
function ENT:SetNextMode(nextMode)
    if nextMode < MODE_INVALID or nextMode > MODE_FLEE2 then
        self.nextMode = MODE_INVALID
    else
        self.nextMode = nextMode
    end
end

----------------------------------------------------
--ENT:Get/SetLastMode()
--get or set the mode we came from
----------------------------------------------------
function ENT:GetLastMode()
    return self.lastMode
end
function ENT:SetLastMode(lastMode)
    if lastMode < MODE_INVALID or lastMode > MODE_FLEE2 then
        self.lastMode = MODE_INVALID
    else
        self.lastMode = lastMode
    end
end

----------------------------------------------------
--ENT:ProjectStalkSound()
--play a sound to the player we are stalking
----------------------------------------------------
function ENT:ProjectStalkSound()
    local snd = STALK_FOUND_SOUNDS[math.random(1, #STALK_FOUND_SOUNDS)]

    sound.Play(snd, self:GetEnemy():GetPos(), 75, 100, 1)
end

----------------------------------------------------
--ENT:Get/SetLastSeenBy()
----------------------------------------------------
function ENT:GetLastSeenBy()
    return self.lastSeenBy
end
function ENT:SetLastSeenBy(lastSeenBy)
    self.lastSeenBy = lastSeenBy
end

----------------------------------------------------
--ENT:GetStalkLocation
--calculate the destination to stalk to
--in general, stalking involves remaining out of sight
--so we target a location behind the player's view
----------------------------------------------------
function ENT:GetStalkLocation()
    local pos = self:GetEnemy():GetPos()
    local aim = self:GetEnemy():GetAimVector()
    --TODO:change this back to behind the target
    return pos + (aim * STALK_COEFFICIENT)
end

----------------------------------------------------
--ENT:HasLOS
--determine if the player has direct line of sight to us
----------------------------------------------------
function ENT:HasLOS(plyr)
	local tr = util.TraceLine({
		start = self:GetPos(),
		endpos = plyr:LocalToWorld(plyr:OBBCenter()),
		filter = {self},
		mask = CONTENTS_SOLID, CONTENTS_OPAQUE, CONTENTS_MOVEABLE
	})

	if tr.Fraction > 0.98 then return true end
	return false
end

----------------------------------------------------
--ENT:PlyrIsStaring
--determine if plyr is looking right at us
----------------------------------------------------
function ENT:PlyrIsStaring(plyr)
	local ViewEnt = plyr:GetViewEntity()
	if ViewEnt == plyr and not self:HasLOS(plyr) then return true end

	local delta = (self:GetPos() - ViewEnt:GetPos()):GetNormalized()
    local eye = ViewEnt:EyeAngles():Forward():GetNormalized()

    local thecos = delta:Dot(eye)

    return thecos > VIEW_EPSILON
end

----------------------------------------------------
--ENT:SeenBy
--determine if any player can see us
--return any arbitrary player that can see us
----------------------------------------------------
function ENT:SeenBy()
	local plys = player.GetAll()
	--print ("how many players? " .. #plys)

	for i, p in ipairs(plys) do
		if not IsValid(p) or not p:Alive() then
		else
			-- if self:PlyrIsStaring(p) then print("can see") else print("can't see") end
			-- if self:HasLOS(p) then print("has los") else print ("no los") end
			if self:PlyrIsStaring(p) and self:HasLOS(p) then
				return p
			end
		end
	end

	return nil
end

----------------------------------------------------
-- ENT:Get/SetEnemy()
-- Simple functions used in keeping our enemy saved
----------------------------------------------------
function ENT:SetEnemy( ent )
	self.Enemy = ent
end
function ENT:GetEnemy()
	return self.Enemy
end

----------------------------------------------------
-- ENT:HaveEnemy()
-- Returns true if we have an enemy
----------------------------------------------------
function ENT:HaveEnemy()
	if ( self:GetEnemy() and IsValid( self:GetEnemy() ) ) then
		return true
	else
		return false
	end
end

----------------------------------------------------
-- ENT:FindEnemy()
-- Returns true and sets our enemy if we find one
----------------------------------------------------
function ENT:FindEnemy()
	print('acquire target')
	local plys = player.GetAll()

	print('have ' .. #plys .. ' players to kill')
	if #plys > 0 then
		--print ('grab a player object ref')
		local ply = plys[math.random(1, #plys)]
		self:SetEnemy(ply)
		return true
	end
	return false
end

----------------------------------------------------
-- ENT:ChaseVector()
-- Works similarly to Garry's MoveToPos function
-- except it will constantly follow the
-- position of the given coordinates.
----------------------------------------------------
function ENT:ChaseVector( coordinates, options )
	local options = options or {}
	local path = Path( "Follow" )

	path:SetMinLookAheadDistance( options.lookahead or 300 )
	path:SetGoalTolerance( options.tolerance or 20 )
    -- Compute the path towards the enemy's position
	path:Compute( self, coordinates )

	if ( !path:IsValid() ) then return "failed" end

	if ( path:IsValid() and self:HaveEnemy() ) then

		path:Update( self )

		if ( options.draw ) then path:Draw() end
		-- If we're stuck then call the HandleStuck function and abandon
		if ( self.loco:IsStuck() ) then
			self:HandleStuck()
			return "stuck"
		end

	end

	return "ok"
end

----------------------------------------------------
-- ENT:Blink(coordinate)
-- move to point on the navmesh closest to coordinate
----------------------------------------------------
function ENT:Blink(coordinate, options)
	local options = options or {}
	local path = Path( "Follow" )

	path:SetMinLookAheadDistance( options.lookahead or 300 )
	path:SetGoalTolerance( options.tolerance or 20 )
	path:Compute( self, coordinate )

	if ( !path:IsValid() ) then return "failed" end

	if ( path:IsValid() and self:HaveEnemy() ) then

		if (options.draw) then path:Draw() end

		if ( self.loco:IsStuck() ) then
			self:HandleStuck()
			return "stuck"
		end

        local delta = Vector(0, 0, self.Width * 2)

		self:SetPos(path:GetEnd() + delta)
	end

	return "ok"
end

----------------------------------------------------
-- ENT:RunBehaviour()
-- Apply our logic
----------------------------------------------------
function ENT:RunBehaviour()
	while (true) do

		if self:GetMode() == MODE_INVALID then
            self:HandleInvalidMode()
		elseif self:GetMode() == MODE_STALK then
            self:HandleStalkMode()
		elseif self:GetMode() == MODE_FLEE then
            self:HandleFleeMode()
        elseif self:GetMode() == MODE_FLEE2 then
            self:HandleFleeMode2()
		elseif self:GetMode() == MODE_KILL then
            self:HandleKillMode()
		else
			self:SetNextMode(MODE_INVALID)
		end

        self:SetLastMode(self:GetMode())
        self:SetMode(self:GetNextMode())

        print('done thinking for now')
        print('-----------------------------------------')

        if CLIENT then
            local en = self:GetEnemy()
            if (en and IsValid(en)) then
                render.DrawLine(en:GetPos(), self:GetPos(), Color(255,255,255), true)
                print ('draw the line...')
            end
        end

        if self:IsOnGround() then print('ON THE GROUND') else print('NOT ON THE GROUND') end

        --coroutine.yield()
		coroutine.wait(0.05)
	end
end

----------------------------------------------------
--ENT:HandleInvalidMode()
--transition away from invalid modes and reinitialize as needed
----------------------------------------------------
function ENT:HandleInvalidMode()
    print('invalid mode, transition to stalking')
    self:SetNextMode(MODE_STALK)
end

----------------------------------------------------
--ENT:HandleStalkMode()
--perform stalking behavior
----------------------------------------------------
function ENT:HandleStalkMode()
    print('stalking mode')

    --in transition logic
    if self:GetLastMode() ~= self:GetMode() then
        self:SetLastSeenBy(nil)
        self:FindEnemy()
        if not self:HaveEnemy() then
            self:SetNextMode(MODE_INVALID)
        else
            print('now tracking ' .. self:GetEnemy():GetName())
            self:ProjectStalkSound()
        end
    end

    --out transition logic
    local seen = self:SeenBy()
    if seen ~= nil then
        self:SetLastSeenBy(seen)
        self:SetNextMode(MODE_FLEE)
    end

    local distance = self:GetStalkLocation():Distance(self:GetPos())
    if distance < STALK_EPSILON then
        self:SetNextMode(MODE_KILL)
    end

    --standard mode logic
    if (self:GetActivity() ~= ACT_WALK_RIFLE) then
        self:StartActivity( ACT_WALK_RIFLE )
    end
    self.loco:SetAcceleration(STALK_ACCELERATION)
    self.loco:SetDesiredSpeed(STALK_SPEED)

    self:ChaseVector(self:GetStalkLocation(), {draw = true})
end

----------------------------------------------------
--ENT:HandleFleeMode()
--senpai is suspicious, run away!
----------------------------------------------------
function ENT:HandleFleeMode()
    print('fleeing mode')
    print('aye, ye found me, but can ye survive?')
    print('oh no, you saw me: ' .. self:GetLastSeenBy():GetName())

    --in transition logic
    if self:GetLastMode() ~= self:GetMode() then
        local effectData = EffectData()
        effectData:SetAttachment(1)
        effectData:SetEntity(self)
        effectData:SetOrigin(self:GetPos())
        effectData:SetNormal(Vector(0,0,1))
        effectData:SetScale(6)

        -- util.Effect("HelicopterMegaBomb", effectData)
        util.Effect("bblob_puffblink", effectData)

        self:SetFlee1AnimEndTime()
        self:SetFlee1BlinkTime()

        self:SetCloaked(true)
        self:CancelDecloak()
        self:DelayedDecloak()
    end

    --out transition logic
    if self:IsFlee1AnimEndTimeUp() then
        self:SetNextMode(MODE_FLEE2)
    end

    --standard mode logic
    self.loco:SetAcceleration(COWER_ACCELERATION)
    if (self:GetActivity() ~= ACT_COWER) then
        self:StartActivity( ACT_COWER )
    end
    if (self:IsFlee1BlinkTime() and not self:HasFlee1Blinked()) then
        self:Blink(self:GetSeenByBlinkAwayTarget(), {draw = true})
        self:SetFlee1Blinked(true)
    end

    self.loco:SetAcceleration(1000)
    self.loco:SetDesiredSpeed(0)

    --out transition cleanup
    if self:GetNextMode() ~= self:GetMode() then
        self:SetFlee1Blinked(false)
    end
end

----------------------------------------------------
--ENT:HandleFleeMode2()
--senpai is suspicious, run away!
----------------------------------------------------
function ENT:HandleFleeMode2()
    print('fleeing mode, part 2: flee harder')

    --in transition logic
    if self:GetLastMode() ~= self:GetMode() then
        if (self:GetActivity() ~= ACT_RUN_RIFLE) then
            self:StartActivity( ACT_RUN_RIFLE )
        end

        self:SetFlee2ReturnToStalkTime()
    end

    --out transition logic
    if self:IsFlee2ReturnToStalk() then
        self:SetNextMode(MODE_STALK)
        self:SetLastSeenBy(nil)
    end

    local seen = self:SeenBy()
    if seen ~= nil then
        self:SetLastSeenBy(seen)
        self:SetNextMode(MODE_FLEE)
    end

    --standard mode logic
    self.loco:SetAcceleration(FLEE_ACCELERATION)
    self.loco:SetDesiredSpeed(FLEE_SPEED)
	self:ChaseVector(self:CalcFleeCoords(), {draw = true})
end

----------------------------------------------------
--ENT:HandleKillMode()
--rid the world of our target
----------------------------------------------------
function ENT:HandleKillMode()
    print('killing mode')

    --print standard game mode logic
    self.loco:SetAcceleration(1000)
    self.loco:SetDesiredSpeed(0)
end

----------------------------------------------------
--register with gmod
----------------------------------------------------
list.Set( "NPC", "bblob_stalker", {
	Name = "Stalker",
	Class = "bblob_stalker",
	Category = "binaryblob"
} )
